const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')

beforeEach(async () => {
  // memasukkan data dummy ke database testing
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("alif233", salt)
  await queryInterface.bulkInsert('user_games', [
    {
      username: "AlifSetiawan",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])
})

afterEach(async () => {
    await sequelize.query(
        "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
      );
    
//   await queryInterface.bulkDelete('user_games', {}, { truncate: true, restartIdentity: true })
})

describe('Login API', () => {
  it('success', (done) => {
    request(app)
    .post('/login')
    .send({
      username: "AlifSetiawan",
      password: "alif233"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('token')
        done()
      }
    })
  })

  // it('Wrong password', (done) => {
  //   request(app)
  //   .post('/login')
  //   .send({
  //     username: "Khoirul",
  //     password: "iyung123"
  //   })
  //   .end((err, res) => {
  //     if (err) {
  //       done(err)
  //     } else {
  //       expect(res.status).toBe(401)
  //       expect(res.body).toHaveProperty('message')
  //       expect(res.body.message).toBe('Invalid username or password')
  //       done()
  //     }
  //   })
  // })

  // it('Wrong username', (done) => {
  //   request(app)
  //   .post('/login')
  //   .send({
  //     username: "khoirulle",
  //     password: "Khoirul123"
  //   })
  //   .end((err, res) => {
  //     if (err) {
  //       done(err)
  //     } else {
  //       expect(res.status).toBe(401)
  //       expect(res.body).toHaveProperty('message')
  //       expect(res.body.message).toBe('Invalid username or password')
  //       done()
  //     }
  //   })
  // })
})