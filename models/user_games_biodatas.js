'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games_biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_games_biodatas.belongsTo(models.user_games, { foreignKey: 'user_game_id', as: 'Id User Game' })
      // define association here
    }
  }
  user_games_biodatas.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    age: DataTypes.INTEGER,
    gender: DataTypes.STRING,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_games_biodatas',
  });
  return user_games_biodatas;
};