const {user_games_histories} = require('../models')

class GamesController {
      static listHistories(req, res) {
        // select list data histories
        user_games_histories.findAll()
          .then((data) => {
            res.status(200).json(data)
          })
          .catch((error) => {
            res.status(500).json(error)
          })
      }
    
}

module.exports = GamesController