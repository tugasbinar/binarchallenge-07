const {user_games_biodatas} = require('../models')

class GamesController {
    static listBiodatas(req, res) {
        // select list data biodatas
        user_games_biodatas.findAll()
          .then((data) => {
            res.status(200).json(data)
          })
          .catch((error) => {
            res.status(500).json(error)
          })
      }
}
module.exports = GamesController