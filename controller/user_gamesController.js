const {user_games} = require('../models')

class GamesController {
    static async create(req, res, next) {
        try {
            if (req.file) {
            req.body.video = `http://localhost:3000/${req.file.filename}`
          }
          await user_games.create({
            username: req.body.username,
            password: req.body.password,
            video: req.body.video
        })
          res.status(200).json({
            message: "succesfully create user"
          })
         } catch (error) {
          next(error)
        }
      }
    
}

module.exports = GamesController